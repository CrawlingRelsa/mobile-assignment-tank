﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PadInfo
{
    public Vector2 axis;

    public enum STATUS
    {
        BEGAN,MOVING, STOP
    }
}
