﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Tank : MonoBehaviour
{
    private Rigidbody rb;
    private Transform turret;
    private Transform hull;
    private bool isMouse;

    public float speedRotation;
    public float speed;
    public float maxSpeed;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        turret = transform.GetChild(0);
        hull = transform.GetChild(1);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveHull();
        if (Input.GetKeyDown(KeyCode.T)) isMouse = !isMouse;
        if (isMouse)
        {
            MoveTurretMouse();
        }
        else
        {
            MoveTurretBtn();
        }
        Debug.Log("Mouse activated: " + isMouse);
    }

    void MoveTurretMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            Vector3 dir = hit.point - transform.position;
            dir.y = 0;
            turret.LookAt(dir);
        }
    }

    void MoveTurretBtn()
    {
        turret.Rotate(Input.GetAxisRaw("HorizontalAlt") * Vector3.up * speedRotation);
    }

    void MoveHull()
    {
        if (rb.velocity.magnitude <= maxSpeed)
        {
            Vector3 direction = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            rb.AddForce(direction * speed);
            hull.LookAt(transform.position + direction);
        }

    }
}
