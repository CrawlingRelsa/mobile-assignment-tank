﻿using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
public class Pad : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IDragHandler
{
    private bool isClicked;
    private Vector2 startPosition;
    private Vector2 currentPosition;
    private RectTransform rectTransform;
    private Camera mainCam;

    //these should be an object with events
    public Vector2 axis;
    public bool readyToShoot;

    public void Start()
    {
        mainCam = FindObjectOfType<Camera>();
        rectTransform = GetComponent<RectTransform>();
        startPosition = mainCam.WorldToScreenPoint(rectTransform.position);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        isClicked = true;
        currentPosition = eventData.position;
        axis = currentPosition - startPosition;
    }

    public void OnDrag(PointerEventData eventData)
    {
        currentPosition = eventData.position;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        isClicked = false;
        readyToShoot = true;
    }

    void Update()
    {
        if (isClicked)
        {
            //the delta vector is divided by the half width of the ui image in order to clamp it correctly
            Vector2 inputDirection = (currentPosition - startPosition) / (rectTransform.rect.width / 2);

            axis = new Vector2(
                //sign of the direction * clamped absolute value of delta (for negative values)
                Mathf.Sign(inputDirection.x) * Mathf.Clamp01(Mathf.Abs(inputDirection.x)), 
                Mathf.Sign(inputDirection.y) * Mathf.Clamp01(Mathf.Abs(inputDirection.y)));
        }
        else
        {
            axis = Vector2.zero;
        }
    }
}
