﻿using UnityEngine;

/*
 * TODO:
 * - manca lo sparo
 * - test su mobile
 */

[RequireComponent(typeof(Rigidbody))]
public class TankMobile : MonoBehaviour
{
    private Rigidbody rb;
    private Transform turret;
    private Transform hull;
    private bool isMouse;
    [Header("Bullet")]
    public GameObject bullet;
    [Header("Speed settings")]
    public float speedRotation;
    public float speed;
    public float maxSpeed;
    [Header("Input Axis Pad")]
    public Pad leftPad;
    public Pad rightPad;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        turret = transform.GetChild(0);
        hull = transform.GetChild(1);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        MoveHull();
        MoveTurretBtn();
        Shoot();


    }

    void Shoot()
    {
        if (rightPad.readyToShoot)
        {
            rightPad.readyToShoot = false;
            Instantiate(bullet, turret.position, turret.rotation);
        }

    }


    void MoveTurretBtn()
    {
        turret.Rotate(rightPad.axis.x * Vector3.up * speedRotation);
    }

    void MoveHull()
    {
        if (rb.velocity.magnitude <= maxSpeed)
        {
            Vector3 direction = new Vector3(leftPad.axis.x, 0, leftPad.axis.y);
            rb.AddForce(direction * speed);
            hull.LookAt(transform.position + direction);
        }

    }
}
